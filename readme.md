vboxkext.sh
==========

bash script for OS X

A simple, single bash script to load/unload/stat the kernel extensions used by Virtual Box on OS X.  Useful for working with multiple VM vendors (VMWare, Parallels) or to save resources when Virtual Box is not being used.  I stole this from  https://gist.github.com/rtgibbons/2024307

Install
------------
download and copy the .sh file to a folder in your path, e.g. /usr/local/bin, or ~/bin  If you don't know what that means, then you may have more to learn about Mac terminal and unix.  See http://software-carpentry.org/3_0/shell01.html  

Usage
-----
from terminal

$>vboxkext load|unload|reload|stat

Motivation
---------

I'll be honest, I don't trust kernel extensions to play nice, especially if they functionally overlap.  Virtual Machine platforms need Kernel Ext to work.  VMWare (don't know about Parallels) loads and unloads them when you start/stop the app but virtual box does not.  This script could be hooked to a script that starts/stops VirtualBox to emulate VMWare's behavior.  

